## Huawei LiteOS贡献代码流程
进行Huawei LiteOS的代码贡献可以遵循以下流程

1. 创建Gitee账号
2. fork LiteOS 源代码
3. 创建开发分支
4. 同步LiteOS主仓库代码到fork的仓库
5. 提交pull request到LiteOS仓库

### 1 创建Gitee账号

由于LiteOS是在Gitee上进行代码管理的，因此代码贡献者也需要在Gitee上注册账户才能贡献代码。
在浏览器输入Gitee.com，然后在其界面上进行账户的注册(如有Gitee账户，则直接使用即可)。

### 2 fork LiteOS源代码

拥有Gitee账户后，则可以将LiteOS仓库fork到自己账户下，步骤如下：

- 首先登录Gitee账户
- 然后在Gitee中找到https://gitee.com/liteos

- 点击fork按钮，将LiteOS的代码fork到自己账户下


点击完成后，稍等一会就会自动跳转到自己账户下的LiteOS位置。


### 3 创建开发分支

在自己的账户下的LiteOS仓库下创建新的开发分支，开发新功能或者修正bug等等。


可以选择不同的分支，默认是master


### 4 同步LiteOS主仓库代码到fork的仓库


### 5 提交pull request到LiteOS仓库
