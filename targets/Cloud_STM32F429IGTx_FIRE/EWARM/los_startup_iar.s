;/******************** (C) COPYRIGHT 2015 STMicroelectronics ********************
;* File Name          : startup_stm32f40_41xxx.s
;* Author             : MCD Application Team
;* @version           : V1.6.0
;* @date              : 10-July-2015
;* Description        : STM32F40xxx/41xxx devices vector table for EWARM toolchain.
;*                      This module performs:
;*                      - Set the initial SP
;*                      - Set the initial PC == _iar_program_start,
;*                      - Set the vector table entries with the exceptions ISR
;*                        address.
;*                      - Configure the system clock and the external SRAM mounted on
;*                        STM324xG-EVAL board to be used as data memory (optional,
;*                        to be enabled by user)
;*                      - Branches to main in the C library (which eventually
;*                        calls main()).
;*                      After Reset the Cortex-M4 processor is in Thread mode,
;*                      priority is Privileged, and the Stack is set to Main.
;********************************************************************************
;*
;* Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
;* You may not use this file except in compliance with the License.
;* You may obtain a copy of the License at:
;*
;*        http://www.st.com/software_license_agreement_liberty_v2
;*
;* Unless required by applicable law or agreed to in writing, software
;* distributed under the License is distributed on an "AS IS" BASIS,
;* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;* See the License for the specific language governing permissions and
;* limitations under the License.
;*
;*******************************************************************************/
;
;
; The modules in this file are included in the libraries, and may be replaced
; by any user-defined modules that define the PUBLIC symbol _program_start or
; a user defined start symbol.
; To override the cstartup defined in the library, simply add your modified
; version to the workbench project.
;
; The vector table is normally located at address 0.
; When debugging in RAM, it can be located in RAM, aligned to at least 2^6.
; The name "__vector_table" has special meaning for C-SPY:
; it is where the SP start value is found, and the NVIC vector
; table register (VTOR) is initialized to this address if != 0.
;
; Cortex-M version
;

    MODULE  ?cstartup

    ;; Forward declaration of sections.
    SECTION CSTACK:DATA:NOROOT(3)
    SECTION LOS_HEAP:DATA:NOROOT(3)

    SECTION .text:CODE:NOROOT(2)
    PUBLIC  __LOS_HEAP_ADDR_START__
    PUBLIC  __LOS_HEAP_ADDR_END__
    EXTERN  __ICFEDIT_region_RAM_end__
    DATA
__LOS_HEAP_ADDR_START__
    DCD     sfb(LOS_HEAP)
__LOS_HEAP_ADDR_END__
    DCD     __ICFEDIT_region_RAM_end__

    SECTION .intvec:CODE:NOROOT(2)

    EXTERN  __iar_program_start
    EXTERN  SystemInit
    EXPORT  Reset_Handler
    PUBLIC  __vector_table

    DATA
__vector_table
    DCD     sfe(CSTACK)
    DCD     Reset_Handler   ; Reset Handler
    DCD     0               ; NMI Handler
    DCD     0               ; Hard Fault Handler
    DCD     0               ; MPU Fault Handler
    DCD     0               ; Bus Fault Handler
    DCD     0               ; Usage Fault Handler
    DCD     0               ; Reserved
    DCD     0               ; Reserved
    DCD     0               ; Reserved
    DCD     0               ; Reserved
    DCD     0               ; SVCall Handler
    DCD     0               ; Debug Monitor Handler
    DCD     0               ; Reserved
    DCD     0               ; PendSV Handler
    DCD     0               ; SysTick_Handler

    THUMB
    PUBWEAK Reset_Handler
    SECTION .text:CODE:REORDER:NOROOT(2)
Reset_Handler
    LDR     R0, =SystemInit
    BLX     R0
    LDR     R0, =__iar_program_start
    BX      R0
    END

